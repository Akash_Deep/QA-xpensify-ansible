Main File : expesify-provision.yml

Parameters need to be passed :

    artifact_id=someartifactid

Roles :

    frontend
    backend

Command Example :

For Frontend :  

    ansible-playbook expensify-provision.yml --extra-vars "artifact_id=55429564" --tags frontend --vault-password-file .vault_pass

For Backend :

    ansible-playbook expensify-provision.yml --extra-vars "artifact_id=55429564" --tags backend --vault-password-file .vault_pass
